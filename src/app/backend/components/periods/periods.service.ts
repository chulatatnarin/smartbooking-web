import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class PeriodsService {
    // pathPrefix: any = `:40014`
    // pathPrefixAuth: any = `:40001/periods`;

    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}`,
    });

    constructor() {
        this.axiosInstance.interceptors.request.use((config) => {
            const token = sessionStorage.getItem('token');

            if (token) {
                config.headers['Authorization'] = `Bearer ${token}`;
            }
            return config;
        });
    }

    async list() {
        const url = `/periods`;
        return this.axiosInstance.get(url);
    }
    async getByHospitalID(id:any) {
        const url = `/periods/getByHospitalID/`+ id;
        return this.axiosInstance.get(url);
    }
    async getById(id: any) {
        const url = `/periods/` + id;
        return this.axiosInstance.get(url);
    }

    async save(data: any) {
        const url = `/periods`;
        return this.axiosInstance.post(url, data);
    }

    async update(id: any, data: any) {
        const url = `/periods/` + id;
        return this.axiosInstance.put(url, data);
    }


}
