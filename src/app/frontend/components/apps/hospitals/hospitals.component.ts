import { Component, OnInit } from '@angular/core';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';

import { LayoutService } from '../../../../layout/service/app.layout.service';

@Component({
    selector: 'app-backend-hospitals',
    templateUrl: './hospitals.component.html',
    styleUrls: ['./hospitals.component.scss'],
})
export class HospitalsComponent {

    listHospitals: any;
    listSlot: any;
    selectedHospitals: any[]= [];
    filterSlots: any;

    checkboxState: boolean[] = [];

    constructor(
        private layoutService: LayoutService,
        private router: Router
    ) {
        this.listSlot = JSON.parse(sessionStorage.getItem('slots') || '[]');
    }

    async ngOnInit() {
        await this._prepareHospital();
    }

    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal(
            (event.target as HTMLInputElement).value,
            'contains'
        );
    }

    _prepareHospital() {
        this.listHospitals = Array.from(
            new Set(this.listSlot.map((event: any) => event.hospital_id))
        ).map((hospital_id: any) => {
            return this.listSlot.find((event: any) => event.hospital_id === hospital_id);
        });
    }

    // _prepareData from slot
    private _prepareData(data: any) {
        this.filterSlots = [];
        this.listSlot.map((item: any) => {
            data.includes(item.hospital_id) ? item.checked = true : item.checked = false;
        });
        this.filterSlots = this.listSlot.filter((item: any) => item.checked === true);
    }

    // go to calendar
    calendar() {
        this._prepareData(this.selectedHospitals);
        sessionStorage.setItem('slots', JSON.stringify(this.filterSlots));
        this.router.navigate(['/frontend/apps/calendar']);
    }

    getHospital(e: any,id:number) {
        if (e.checked.length > 0) {
            this.selectedHospitals.push(id);
        } else {
            this.selectedHospitals.splice(this.selectedHospitals.indexOf(id), 1);
        }
    }

    clear() {
        this.selectedHospitals = [];
    }

    activities() {
		this.router.navigate(['/frontend/apps/activity']);
    }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({ behavior: 'smooth' });
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image =
            this.layoutService.config.colorScheme === 'dark'
                ? 'line-effect-dark.svg'
                : 'line-effect.svg';

        return { 'background-image': 'url(' + path + image + ')' };
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }
}
