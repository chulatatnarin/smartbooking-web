import { Component, OnInit } from '@angular/core';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';

import { LayoutService } from '../../../../layout/service/app.layout.service';
import { AppointCardService } from './appoint-card.service';
import { DateTime } from 'luxon';

@Component({
    selector: 'app-appoint-card',
    templateUrl: './appoint-card.component.html',
    styleUrls: ['./appoint-card.component.scss'],
})
export class AppointCardComponent {

    blockedPanel: boolean = false;

    displayForm: boolean = false;

    reserveData: any = [];
    reserveId: number = 0;
    checkboxState: boolean[] = [];
    hospital_name = '';
    service_type_name = '';
    period_name = '';
    slot_date = '';
    slot_time = '';
    customer_name = '';
    customer_tel = '';
    chronic_detail = '';
    cid = '';
    passport = '';
    user_name = '';
    reserve_date = '';
    user_tel = '';
    remarks = '';

    hospitalsStatus = [
        { label: 'Active', value: true },
        { label: 'Inactive', value: false },
    ];

    constructor(
        private layoutService: LayoutService,
        private appointService: AppointCardService,
        private router: Router
    ) {
        this.reserveId = sessionStorage.getItem('reserveId') ? parseInt(sessionStorage.getItem('reserveId') || '0') : 0;
    }

    async ngOnInit() {
        await this.getReserve(this.reserveId);
        // console.log(this.reserveData);
        this.hospital_name = this.reserveData.hospital.hospital_name;
        this.service_type_name = this.reserveData.service_type.service_type_name;
        this.period_name = this.reserveData.period.period_name;
        this.slot_date = this.thaiDate(this.reserveData.slot.slot_date) || '';
        this.customer_name = this.reserveData.customer.customer_name;
        this.customer_tel = this.reserveData.customer.phone_number;
        this.chronic_detail = this.reserveData.customer.chronic;
        this.cid = this.reserveData.customer.cid;
        this.passport = this.reserveData.customer.passport;
        this.user_name = this.reserveData.user.title + this.reserveData.user.fullname;
        this.reserve_date = this.thaiDate(this.reserveData.reserve_date);
        this.user_tel = this.reserveData.user.phone_number;
        this.remarks = this.reserveData.service_type.note;
    }

    async getReserve(id: number) {
        this.blockedPanel = true;
            await this.appointService.getById(id).then((res) => {
            this.reserveData = res.data.results[0];
        });

        if(this.reserveData.slot_id) {
            await this.getSlot(this.reserveData.slot_id);
        }
        if(this.reserveData.customer_id) {
            await this.getCustomer(this.reserveData.customer_id);
        }
        if(this.reserveData.slot.period_id) {
            await this.getPeriod(this.reserveData.slot.period_id);
        }
        if(this.reserveData.slot.service_type_id) {
            await this.getServiceType(this.reserveData.slot.service_type_id);
        }
        if(this.reserveData.slot.hospital_id) {
            await this.getHospital(this.reserveData.slot.hospital_id);
        }
        if(this.reserveData.user_id) {
            await this.getUser(this.reserveData.user_id);
        }
        this.blockedPanel = false;
    }

    async getHospital(id:number){
        await this.appointService.getHospitalById(id).then((res) => {
            this.reserveData.hospital = res.data.results[0];
        });
    }

    async getSlot(id:number){
        await this.appointService.getSlotById(id).then((res) => {
            this.reserveData.slot = res.data.results[0];
        });
    }

    async getCustomer(id:number){
        await this.appointService.getCustomerById(id).then((res) => {
            this.reserveData.customer = res.data.results[0];
        });
    }

    async getPeriod(id:number){
        await this.appointService.getPeriodById(id).then((res) => {
            this.reserveData.period = res.data.results[0];
        });
    }

    async getServiceType(id:number){
        await this.appointService.getServiceTypeById(id).then((res) => {
            this.reserveData.service_type = res.data.results[0];
        });
    }

    async getUser(id:number){
        await this.appointService.getProfilesById(id).then((res) => {
            this.reserveData.user = res.data.results[0];
        });
    }

    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal(
            (event.target as HTMLInputElement).value,
            'contains'
        );
    }

    _prepareHospital() {

    }

    // _prepareData from slot
    private _prepareData(data: any) {

    }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({ behavior: 'smooth' });
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image =
            this.layoutService.config.colorScheme === 'dark'
                ? 'line-effect-dark.svg'
                : 'line-effect.svg';

        return { 'background-image': 'url(' + path + image + ')' };
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }

    thaiDate(date: string) {
        let thaidate = DateTime.fromISO(date).setLocale('th-TH');
        let day = thaidate.day;
        let monthName = thaidate.monthLong;
        let thaiYear = thaidate.year + 543;
        let datethai = day + ' ' + monthName + ' ' + thaiYear;
        return datethai;
    }

    clearData() {
        this.reserveData = [];
    }

    goBack() {
        this.router.navigate(['/frontend/apps/list-booking']);
    }
}
