import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class MessageService {
    messages: any | undefined;
    constructor() {}

    successMessages(detail: any) {
        this.messages = [
            { severity: 'success', summary: 'Success', detail: detail },
        ];
    }
    errorMessages(detail: any) {
        this.messages = [
            { severity: 'error', summary: 'Error', detail: detail },
        ];
    }

    infoMessages(detail: any) {
        this.messages = [{ severity: 'info', summary: 'Info', detail: detail }];
    }

    warningMessages(detail: any) {
        this.messages = [
            { severity: 'warn', summary: 'Warning', detail: detail },
        ];
    }
}
