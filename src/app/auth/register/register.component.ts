import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from './register.service';

@Component({
    templateUrl: './register.component.html',
})
export class RegisterComponent {
    jwtHelper: JwtHelperService = new JwtHelperService();
    public validateForm!: FormGroup;
    public passwordVisible: Boolean = false;

    // messages: Message[] | undefined;
    messages: any | undefined;

    // user data
    title: string = 'คุณ';
    role_id: number = 4;
    position: string = 'ประชาชน';
    hospital_id: number = 4;
    service_id: number = 1;

    display: boolean = true;
    isValidateUsername: boolean = false;
    isPasswordValid: boolean = false;

    constructor(
        public register: FormBuilder,
        private layoutService: LayoutService,
        private registerService: RegisterService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.validateForm = this.register.group({
            username: ['', [Validators.required]],
            password: ['', [Validators.required]],
            passwordConfirm: ['', [Validators.required]],
            fullname: ['', [Validators.required]],
            cid: ['', [Validators.required]],
        });
    }

    get filledInput(): boolean {
        return this.layoutService.config.inputStyle === 'filled';
    }

    async onSubmit() {
        for (const i in this.validateForm.controls) {
            this.validateForm.controls[i].markAsDirty();
            this.validateForm.controls[i].updateValueAndValidity();
        }

        if (this.validateForm.status == 'INVALID') {
            this.showMessages('error', 'Error', 'กรุณากรอกข้อมูลให้ครบถ้วน');
            return;
        }

        if (this.validateForm.status == 'VALID') {
            let { username, password, passwordConfirm, fullname, cid } =
                this.validateForm.value;

            username = username.replaceAll('-','');
            cid = cid.replaceAll('-','');
            if(username.length != 10){
                this.isValidateUsername = false;
                this.showMessages('error', 'Error', 'เบอร์โทรศัพท์ไม่ถูกต้อง');
                return;
            } 
            if(cid.length != 13){
                this.isValidateUsername = false;
                this.showMessages('error', 'Error', 'เลขบัตรประชาชนไม่ถูกต้อง');
                return;
            } 
            if (password != passwordConfirm) {
                this.isPasswordValid = false;
                this.showMessages('error', 'Error', 'รหัสผ่านไม่ตรงกัน');
                return;
            }
            if (password.length < 6) {
                this.isPasswordValid = false;
                this.showMessages(
                    'error',
                    'Error',
                    'รหัสผ่านต้องมีความยาวมากกว่า 6 ตัวอักษร'
                );
                return;
            }
            console.log(
                'login:',
                username,
                password,
                passwordConfirm,
                fullname,
                cid
            );
            const userData = {
                username: username,
                password: password,
                role_id: this.role_id,
            };
            console.log('user:', userData);
            try {
                let res: any = await this.registerService.save(userData);
                if (res.data.ok) {
                    const profileData = {
                        user_id: res.data.results[0].user_id,
                        title: this.title,
                        fullname: fullname,
                        position: this.position,
                        phone_number: username,
                        cid: cid,
                        hospital_id: this.hospital_id,
                    };
                    console.log('profile:', profileData);
                    let profile: any = await this.registerService.saveProfiles(
                        profileData
                    );
                    if (profile.data.ok) {
                        console.log('go login');
                        this.login(username, password);
                        this.validateForm.reset();
                    }
                }
            } catch (error: any) {
                console.log(error);
            }
        } else {
            alert('กรุณาตรวจสอบข้อมูลให้ถูกต้อง');
        }
    }
    async checkUsername(data: any) {
        let { username } = this.validateForm.value;
        if (
            username != '' &&
            username != null &&
            username != undefined &&
            username.length > 4
        ) {
            const res: any = await this.registerService.checkUsername(username);
            if (res.data.ok) {
                this.isValidateUsername = false;
                this.showMessages(
                    'error',
                    'Error',
                    'มีเบอร์โทรนี้ในระบบแล้ว'
                );
            } else {
                this.isValidateUsername = true;
                this.showMessages('success', 'Success', 'ชื่อผู้ใช้งานนี้ใช้ได้');
            }
        }
    }

    async login(username: any, password: any) {
        try {
            const response: any = await this.registerService.login(
                username,
                password
            );
            console.log('user:', response);
            if (response.data) {
                // set token
                let token = response.data.results.accessToken;
                // set user role
                let userRole = response.data.results.info.role.role_id;
                // set user login name
                let userLoginName = response.data.results.info.name;
                // set user role name
                let userRoleNam = response.data.results.info.role.role_name;
                //set hospital id
                let hospitalId = response.data.results.info.hospital_id;
                // set service id
                let serviceId = response.data.results.info.service_id;

                // session storage token, user_role, userLoginName
                sessionStorage.setItem('token', token);
                sessionStorage.setItem('userId', response.data.results.info.user_id);
                sessionStorage.setItem('userRole',userRole);
                sessionStorage.setItem('userLoginName', userLoginName);
                sessionStorage.setItem('userRoleNam', userRoleNam);
                sessionStorage.setItem('hospitalId', hospitalId);
                sessionStorage.setItem('serviceId', serviceId);

                const decoded = this.jwtHelper.decodeToken(token);
                if (userRole == 1 || userRole == 2 || userRole == 3) {
                    // backend page
                    this.router.navigate(['/backend']);
                } else if (userRole == 4) {
                    // main page
                    this.router.navigate(['/frontend']);
                } else {
                    // not found page
                    this.router.navigate(['/notfound']);
                }
            }
        } catch (error) {
            this.showMessages('error','Error','ชื่อผู้ใช้งาน/รหัสผ่าน ไม่ถูกต้อง');
        }
    }

    goLogin() {
        this.display = false;
        this.router.navigate(['/login']);
    }

    // alert message
    showMessages(severity: any, summary: any, detail: any) {
        this.messages = [
            { severity: severity, summary: summary, detail: detail },

            // { severity: 'success', summary: 'Success', detail: 'Message Content' },
            // { severity: 'info', summary: 'Info', detail: 'Message Content' },
            // { severity: 'warn', summary: 'Warning', detail: 'Message Content' },
            // { severity: 'error', summary: 'Error', detail: 'Message Content' }
        ];
    }

    clearMessages() {
        this.messages = [];
    }
}
